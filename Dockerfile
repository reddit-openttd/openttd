#FROM quay.io/pypa/manylinux_2_28_${TARGETARCH} as build
FROM ubuntu:noble as build

ARG OPENTTD_VERSION=15.0-beta1
ARG GFX_VERSION=0.5.5
ARG TARGETARCH

ENV DEBIAN_FRONTEND=noninteractive

RUN mkdir /src /build /app1

WORKDIR /build

# Install dependencies
# perl-IPC-Cmd, wget, and zip are needed to run vcpkg.
# autoconf-archive is needed to build ICU.
RUN apt-get update && apt-get install -y \
    build-essential \
    autoconf-archive \
    libipc-run-perl \
    git \
    zip \
    cmake \
    curl \
    wget \
    perl \
    ninja-build \
    autotools-dev \
    libglib2.0-dev

RUN echo "${TARGETARCH}}: Cmake version: $(cmake --version)"

RUN git clone --depth 1 --branch ${OPENTTD_VERSION} https://github.com/OpenTTD/OpenTTD.git /src

# Install RUST toolchain
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

ENV PATH="/root/.cargo/bin:${PATH}"

# aclocal looks first in /usr/local/share/aclocal, and if that doesn't
# exist only looks in /usr/share/aclocal. We have files in both that
# are important. So copy the latter to the first, and we are good to
# go.
# RUN cp /usr/share/aclocal/* /usr/local/share/aclocal/

# The yum variant of fluidsynth depends on all possible audio drivers,
# like jack, ALSA, pulseaudio, etc. This is not really useful for us,
# as we route the output of fluidsynth back via our sound driver, and
# as such do not use these audio driver outputs at all.
# The vcpkg variant of fluidsynth depends on ALSA. Similar issue here.
# So instead, we compile fluidsynth ourselves, with as few
# dependencies as possible. We do it before anything else is installed,
# to make sure it doesn't pick up on any of the drivers.
RUN wget https://github.com/FluidSynth/fluidsynth/archive/v2.3.3.tar.gz && \
    tar xf v2.3.3.tar.gz && \
    cd fluidsynth-2.3.3 && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=/usr && \
    cmake --build . -j $(nproc) && \
    cmake --install .

# These video libs are to make sure the SDL version of vcpkg adds
# video-support; these libraries are not added to the resulting
# binary, but the headers are used to enable them in SDL.
# RUN apt-get install -y \
#     libX11-devel \
#     libXcursor-devel \
#     libXext-devel \
#     libXfixes-devel \
#     libXi-devel \
#     libxkbcommon-devel \
#     libXrandr-devel \
#     libXScrnSaver-devel \
#     mesa-libEGL-devel \
#     mesa-libGL-devel \
#     mesa-libGLES-devel \
#     wayland-devel \
#     wayland-protocols-devel

# We use vcpkg for our dependencies, to get more up-to-date version.
RUN if [ "$TARGETARCH" = "arm64" ];  then export VCPKG_FORCE_SYSTEM_BINARIES=1; fi && \
 git clone https://github.com/microsoft/vcpkg /vcpkg && \
 cd /vcpkg && \
 ./bootstrap-vcpkg.sh -disableMetrics

RUN cargo install --locked dump_syms

#### BUILD ####
RUN  if [ "$TARGETARCH" = "arm64" ];  then export VCPKG_FORCE_SYSTEM_BINARIES=1; fi && \
 cmake /src \
  -DCMAKE_TOOLCHAIN_FILE=/vcpkg/scripts/buildsystems/vcpkg.cmake \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DGLOBAL_DIR=/app \
  -DCMAKE_INSTALL_PREFIX=/app \
  -DOPTION_PACKAGE_DEPENDENCIES=ON || cat /build/vcpkg-manifest-install.log
  # EOF

RUN echo "Building on 16 cores" && cmake --build . -j 16 --target openttd

RUN make install

# RUNTIME
FROM ubuntu:noble

ARG GFX_VERSION=0.5.5
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
  wget \
  git \
  unzip \
  libgomp1 \
  libglib2.0 \
  xz-utils

COPY --from=build /app /app

RUN mkdir /config /extract
WORKDIR /extract

RUN wget -q http://bundles.openttdcoop.org/opengfx/releases/${GFX_VERSION}/opengfx-${GFX_VERSION}.zip \
    && unzip opengfx-${GFX_VERSION}.zip \
    && tar -xf opengfx-${GFX_VERSION}.tar \
    && mv opengfx-${GFX_VERSION}/* /app/baseset

RUN groupadd -r --gid 4101 openttd && \
  adduser --home /config --system --uid 4101 --gid 4101 openttd
    
RUN chmod u+x /app/openttd
RUN chown -R openttd:openttd /config /app

WORKDIR /app
VOLUME /config

USER openttd

EXPOSE 3977/tcp
EXPOSE 3979/tcp
EXPOSE 3979/udp

ENTRYPOINT [ "/app/openttd" ]
